debug:
	g++ -g -Wall -std=c++11 genxlsx.cpp Template.cpp -o genxlsx -lzip
all:
	g++ -c -s -std=c++11 -O2 -Wall genxlsx.cpp Template.cpp
	g++  -std=c++11 *.o -o genxlsx -lzip
clean:
	rm -f *.o genxlsx