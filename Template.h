#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#define FILESHEET "Template/xl/worksheets/sheet1.xml"
#define ZIPDIR "Template"

class xslxClass{
    protected:
        int countRow=1;
        std::vector<std::string> stmpl;
    public:
        void OpenSheet(std::string beetween);
        void CloseSheet();
        void OpenRow();
        void CloseRow();
        void AddCell(std::string cell, std::string value);
        void GenXLSX(std::string file);
};