#include "Template.h"
#include <iostream>
#include <string>

int main(int argc, char* argv[]){
    if(argc!=2) {
        std::cout << "Пример использования \n\t genxlsx prim.xlsx" << std::endl;
        return 1;
    }
    xslxClass sheet1;
    sheet1.OpenSheet("A1:C2");
        sheet1.OpenRow();
            sheet1.AddCell("A1","1");
            sheet1.AddCell("B1","2");
            sheet1.AddCell("C1","3");
        sheet1.CloseRow();
        sheet1.OpenRow();
            sheet1.AddCell("A2","4");
            sheet1.AddCell("B2","5");
            sheet1.AddCell("C2","6");
        sheet1.CloseRow();
    sheet1.CloseSheet();
    sheet1.GenXLSX(argv[1]);
    return 0;
}