#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "zip.cpp"
#include "Template.h"

#define FILESHEET "Template/xl/worksheets/sheet1.xml"
#define ZIPDIR "Template"



        void xslxClass::OpenSheet(std::string beetween){   
            stmpl.push_back("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\"><sheetPr filterMode=\"false\"><pageSetUpPr fitToPage=\"false\"/></sheetPr><dimension ref=\"");
            stmpl.push_back(beetween);
            stmpl.push_back("\"/><sheetViews><sheetView showFormulas=\"false\" showGridLines=\"true\" showRowColHeaders=\"true\" showZeros=\"true\" rightToLeft=\"false\" tabSelected=\"true\" showOutlineSymbols=\"true\" defaultGridColor=\"true\" view=\"normal\" topLeftCell=\"A1\" colorId=\"64\" zoomScale=\"100\" zoomScaleNormal=\"100\" zoomScalePageLayoutView=\"100\" workbookViewId=\"0\"><selection pane=\"topLeft\" activeCell=\"C2\" activeCellId=\"0\" sqref=\"C2\"/></sheetView></sheetViews><sheetFormatPr defaultRowHeight=\"12.8\" zeroHeight=\"false\" outlineLevelRow=\"0\" outlineLevelCol=\"0\"></sheetFormatPr><cols><col collapsed=\"false\" customWidth=\"false\" hidden=\"false\" outlineLevel=\"0\" max=\"1025\" min=\"1\" style=\"0\" width=\"11.52\"/></cols><sheetData>");
        }
        void xslxClass::CloseSheet(){
            std::string Footer="</sheetData><printOptions headings=\"false\" gridLines=\"false\" gridLinesSet=\"true\" horizontalCentered=\"false\" verticalCentered=\"false\"/><pageMargins left=\"0.7875\" right=\"0.7875\" top=\"1.05277777777778\" bottom=\"1.05277777777778\" header=\"0.7875\" footer=\"0.7875\"/><pageSetup paperSize=\"1\" scale=\"100\" firstPageNumber=\"1\" fitToWidth=\"1\" fitToHeight=\"1\" pageOrder=\"downThenOver\" orientation=\"portrait\" blackAndWhite=\"false\" draft=\"false\" cellComments=\"none\" useFirstPageNumber=\"true\" horizontalDpi=\"300\" verticalDpi=\"300\" copies=\"1\"/><headerFooter differentFirst=\"false\" differentOddEven=\"false\"><oddHeader>&amp;C&amp;&quot;Times New Roman,Regular&quot;&amp;12&amp;A</oddHeader><oddFooter>&amp;C&amp;&quot;Times New Roman,Regular&quot;&amp;12Page &amp;P</oddFooter></headerFooter></worksheet>";
            stmpl.push_back(Footer);
        }

        void xslxClass::OpenRow(){
            stmpl.push_back("<row r=\""+std::to_string(countRow)+"\" customFormat=\"false\" ht=\"12.8\" hidden=\"false\" customHeight=\"false\" outlineLevel=\"0\" collapsed=\"false\">");
            this->countRow++;
        }
        void xslxClass::CloseRow(){
            stmpl.push_back("</row>");
        }

        void xslxClass::AddCell(std::string cell, std::string value){
            stmpl.push_back("<c r=\""+cell+"\" s=\"0\" t=\"n\"><v>"+value+"</v></c>");
        }

        void xslxClass::GenXLSX(std::string file){
            std::ofstream sheet;
            try{
                sheet.open (FILESHEET);
                unsigned int vector_size =stmpl.size();
                for (int i = 0; i < vector_size; i++) {
                    //std::cout << stmpl[i] << std::endl;
                    sheet << stmpl[i];
                }
            sheet.close();
            } catch (const std::ios_base::failure& e) {
                std::cout << "Не могу открыть файл для записи Template/xl/worksheets/sheet1.xml" << std::endl;
                return;
            }
            zip_directory(ZIPDIR, file);

        }
